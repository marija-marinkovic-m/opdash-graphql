import React from 'react';

import { NextPageContext } from 'next';
import Router from 'next/router';

import { checkUserAuth } from '../services/auth-token';

const loginPath = '/login?redirected=true';

const withPrivateRoute = PrivateComponent => {
  const hocComponent = ({ ...props }) => <PrivateComponent {...props} />;

  hocComponent.getInitialProps = async (ctx: NextPageContext) => {
    const auth = await checkUserAuth(ctx);
    if (!auth.loggedIn) {
      if (ctx.res) {
        ctx.res.writeHead(302, {
          Location: loginPath
        });
        ctx.res.end();
      } else {
        Router.replace(loginPath);
      }
    } else if (PrivateComponent.getInitialProps) {
      const wProps = await PrivateComponent.getInitialProps(auth);
      return { auth, ...wProps };
    }

    return { auth };
  };

  return hocComponent;
};

export default withPrivateRoute;
