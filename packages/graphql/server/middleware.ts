import cookieParser from 'cookie-parser';
import cors from 'cors';
import express from 'express';
import requestIp from 'request-ip';

import config, { Environment } from '../config';
import { createLogEntry } from '../logger';

export default ({
  build_number,
  environment,
  mountPath,
  version
}: {
  mountPath: string;
  version: string;
  environment: string;
  build_number: string;
}) => {
  const middleware = express();

  middleware.use(cors());
  middleware.use(cookieParser());

  middleware.use((req, res, next) => {
    req.headers['x-forwarded-for'] = requestIp.getClientIp(req);
    if (req.cookies.authToken) {
      req.headers['x-auth-token'] = req.cookies.authToken;
    }
    next();
  });

  // Load balancer health check
  middleware.get('/health/lb', (req, res) => {
    res.status(200).end();
  });

  middleware.get('/health/check', (req, res) => {
    res.set('Cache-Control', 'no-cache');
    const appStatus = {
      version,
      environment,
      build_number
    };
    res.status(200).send(appStatus);
  });

  middleware.get(`${mountPath}*`, (req, res, next) => {
    if (config.environment === Environment.production) {
      return next();
    }

    if (req.header(config.stAuthHeader) === config.stAuth) {
      return next();
    }

    res.status(401).send();
  });

  // Error handler to get express errors
  middleware.use((err, req, res, next) => {
    createLogEntry('error', err.toString(), {
      context: {
        path: req.path
      }
    });
    return next(err);
  });

  return middleware;
};
