import { gql } from 'apollo-server';

export default gql`
  type Tour {
    name: String
  }
`;
