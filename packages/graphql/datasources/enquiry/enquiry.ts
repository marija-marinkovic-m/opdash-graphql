import { RequestOptions } from 'apollo-datasource-rest';
import jwt, { JsonWebTokenError } from 'jsonwebtoken';
import { get, has } from 'lodash';

import config from '../../config';
import RestAPI from '../base';

interface Passenger {
  'first-name': string;
  'last-name': string;
  email: string;
  'phone-number': string;
  'date-of-birth': string;
  address: string;
  city: string;
  'postal-or-zip-code': string;
  'state-or-province': string;
  country: string;
  nationality: string;
  gender: string;
  'billing-country': string;
  'billing-postal-code': string;
}
export interface EnquiryModel {
  id: number;
  object_id: number;
  date: number;
  email: string;
  phone: string;
  pax: number;
  location: string;
  source: string;
  comment: string;
  added: number;
  status: number;
  payment_provider: string;
  payment_provider_status: string;
  payment_provider_customer: number;
  type: number;
  per_person_currency: string;
  per_person_value: number;
  per_person_base: number;
  per_person_discount_operator: number;
  promotion_id: number;
  per_person_promotion: number;
  promo_code_id: number;
  promo_code_discount: number;
  referral_discount: number;
  traveller_id: number;
  payment_provider_full_payment_date: string;
  payment_provider_full_payment_status: string;
  payment_provider_default_method: number;
  booking_confirmation_id: string;
  hold_period_hours: number;
  allow_invoice_download: number;
  whitelabel_partner: number;
  cancel_reason: string | number;
  cancel_reason_text: string;
  asana_task: string;
  tourcms_voucher_url: string;
  approval_status: number;
  approval_status_updated_at: string;
  is_cs_enquiry: boolean;
  ip_continent: number;
  book_now_url: string;
  invoice_download_url: string;
  total_value: number;
  installment_payments: boolean;
  brochure_price: number;
  hold_expire_at: number;
  passengers: Passenger[];
  confirmed_at: string;
  type_label: string;
  status_label: string;
  is_booking: boolean;
  is_booking_request: boolean;
}

export default class EnquiryAPI extends RestAPI {
  baseURL = `${process.env.DATASOURCE_ENQUIRIES_API}/v1/`;

  willSendRequest(request: RequestOptions) {
    const verified = jwt.verify(
      this.context.authToken,
      process.env.JWT_SECRET || ''
    );

    if (has(verified, 'operatorId')) {
      request.params.set('operator_id', get(verified, 'operatorId'));
    } else {
      throw new JsonWebTokenError('No operator specified');
    }

    if (config.internalApiKey) {
      request.headers.set(
        'Authorization',
        `TourRadar ${config.internalApiKey}`
      );
    }
    if (config.stAuth) {
      request.headers.set(config.stAuthHeader, config.stAuth);
    }
    super.willSendRequest(request);
  }

  async getEnquiries(): Promise<EnquiryModel[]> {
    return this.get<EnquiryModel[]>(`enquiries`, 'limit=1');
  }
}
