import { RESTDataSource, RequestOptions } from 'apollo-datasource-rest';
import { ApolloError } from 'apollo-server';
import { get } from 'lodash';

import config from '../config';
import { createLogEntry, LOG_MESSAGES } from '../logger';

import { throwApiError } from './utils';

export default abstract class RestAPI extends RESTDataSource {
  willSendRequest(request: RequestOptions) {
    const { method, path, params } = request;

    request.headers.set('X-APP-NAME', config.appName);
    request.headers.set('X-APP-VERSION', config.appVersion);
    request.headers.set('X-APP-ENV', config.environment);
    request.headers.set('X-Forwarded-For', this.context.ipAddress);

    createLogEntry('info', 'Calling external API', {
      context: {
        method,
        uri: `${this.baseURL}${path}`,
        params,
        application: {
          name: this.context.appName,
          version: this.context.appVersion
        }
      }
    });
  }

  didEncounterError(error: ApolloError) {
    const status = get(error, 'response.status');
    const logLevel = 422 === status ? 'info' : 'warn';
    const logMessage =
      422 === status
        ? LOG_MESSAGES.invalidInput
        : 401 === status
        ? LOG_MESSAGES.unauth
        : LOG_MESSAGES.apiError;

    createLogEntry(logLevel, logMessage, {
      context: {
        error,
        application: {
          name: this.context.appName,
          version: this.context.appVersion
        }
      }
    });

    throwApiError(error);
  }
}
