import unfetch from 'isomorphic-unfetch';
import jwt from 'jsonwebtoken';
import { has } from 'lodash';
import { NextPageContext } from 'next';
import { parseCookies } from 'nookies';

export const TOKEN_PATH = 'authToken';
export interface AuthData {
  loggedIn: boolean;
  message?: string;
}
export interface DecodedToken {
  readonly exp: number;
  readonly username: string;
}

export default class AuthToken {
  readonly decodedToken: DecodedToken;

  constructor(readonly store?: { [key: string]: string }) {
    this.decodedToken = { username: '', exp: 0 };
    if (has(store, TOKEN_PATH)) {
      this.decodedToken = jwt.decode(store[TOKEN_PATH]) as DecodedToken;
    }
  }

  get expiresAt(): Date {
    return new Date(this.decodedToken.exp * 1000);
  }

  get isExpired(): boolean {
    return new Date() > this.expiresAt;
  }

  get isValid(): boolean {
    return !this.isExpired;
  }
}

export const checkUserAuth = async (ctx: NextPageContext) => {
  let auth: AuthData;
  if (ctx.res) {
    const authInstance = new AuthToken(parseCookies(ctx));
    auth = {
      loggedIn: authInstance.isValid
    };
  } else {
    // because token is httpOnly
    const status = await unfetch('/api/token', { method: 'GET' });
    auth = await status.json();
  }

  return auth;
};
