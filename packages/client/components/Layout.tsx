import React, { FunctionComponent } from 'react';

import { AuthData } from '../services/auth-token';

import styles from './Layout.module.scss';
import Nav from './Nav';

interface Props {
  auth?: AuthData;
}
const Layout: FunctionComponent<Props> = props => (
  <div className={styles.wrapper}>
    <Nav loggedIn={props.auth?.loggedIn} />
    {props.children}
  </div>
);

export default Layout;
