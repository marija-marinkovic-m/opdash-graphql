import React, { useState, FunctionComponent, ChangeEvent } from 'react';

import { useMutation } from '@apollo/react-hooks';
import {
  LoginPayload,
  Session
} from '@opdash-graphql/graphql/__generated__/types';
import fetch from 'isomorphic-unfetch';
import { useRouter } from 'next/router';

import Button from '../components/Button';
import { AUTH_LOGIN } from '../data/auth';
import { withApollo } from '../lib/apollo';

const Login: FunctionComponent = () => {
  const [inputs, setInputs] = useState<LoginPayload>({
    username: '',
    password: ''
  });

  const [loginUser] = useMutation<{ login: Session }, LoginPayload>(AUTH_LOGIN);

  const router = useRouter();

  const handleSubmit = async e => {
    e.preventDefault();

    try {
      const response = await loginUser({
        variables: {
          username: inputs.username,
          password: inputs.password
        }
      });

      if (response && response.data.login) {
        const postLogin = await fetch('/api/token', {
          method: 'POST',
          body: JSON.stringify({ authToken: response.data.login.token })
        });

        try {
          const postLoginStatus = await postLogin.json();
          if (postLoginStatus) {
            const { loggedIn, message } = postLoginStatus;

            alert(
              `Login ${loggedIn ? 'was successful' : 'failed'}. ${
                message ? message : ''
              }`
            );

            return router.push('/ssr');
          }
        } catch (err) {
          alert(`Could not finalize the login: ${JSON.stringify(err)}`);
        }
      }
    } catch (errors) {
      // @todo
      alert('Invalid username/password');
    }
  };

  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    e.persist();
    setInputs({
      ...inputs,
      [e.target.name]: e.target.value
    });
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <div>
          <input
            type="text"
            name="username"
            value={inputs.username}
            onChange={handleInputChange}
            placeholder="Username"
            required
          />
        </div>

        <div>
          <input
            type="password"
            name="password"
            value={inputs.password}
            onChange={handleInputChange}
            placeholder="Password"
            required
          />
        </div>

        <Button type="primary" onClick={handleSubmit}>
          Login
        </Button>
      </form>
    </>
  );
};

export default withApollo()(Login);
