import InternalRestAPI from '../rest';

export interface BasicLoginInput {
  username: string;
  password: string;
}

interface Session {
  user: {
    id: number;
    username: string;
    email: string;
  };
  token: string;
  isAdmin: boolean;
  isTeamMember: boolean;
}

export default class AuthAPI extends InternalRestAPI {
  async login(params: BasicLoginInput) {
    const response = await this.post<Session>('auth/login', params);
    return response;
  }
}
