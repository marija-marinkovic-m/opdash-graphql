import nextApp from '@opdash-graphql/client';
import startGraphQLProcess from '@opdash-graphql/graphql';

const handle: any = nextApp.getRequestHandler();

nextApp.prepare().then(() => {
  const { app } = startGraphQLProcess();
  app.all('*', handle);
});

