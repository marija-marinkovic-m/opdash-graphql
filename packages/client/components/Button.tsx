import React, {
  FunctionComponent,
  AnchorHTMLAttributes,
  ButtonHTMLAttributes
} from 'react';

import cs from 'classnames';

import styles from './Button.module.scss';

export const tuple = <T extends string[]>(...args: T) => args;
const ButtonTypes = tuple('primary', 'secondary', 'tertiary');
export type ButtonType = typeof ButtonTypes[number];

interface Props
  extends Omit<
    Partial<AnchorHTMLAttributes<any> & ButtonHTMLAttributes<any>>,
    'type'
  > {
  type?: ButtonType;
}

const PREFIX = 'scout-btn';

const Button: FunctionComponent<Props> = ({
  type = 'primary',
  children,
  className,
  href,
  ...rest
}) => {
  const classes = cs(styles[PREFIX], className, {
    [styles[`${PREFIX}__${type}`]]: type
  });

  if (href) {
    return (
      <a {...rest} href={href} className={classes}>
        {children}
      </a>
    );
  }

  return (
    <button {...rest} className={classes}>
      {children}
    </button>
  );
};

export default Button;
