import request from 'supertest';

import getMiddleware from './middleware';

const serverSettings = {
  build_number: '2',
  environment: 'st',
  version: '12',
  mountPath: '/test'
};

test('it should respond on health check requests', async () => {
  await request(getMiddleware(serverSettings)).get('/health/lb').expect(200);
});

test('it should respond with current server settings', async () => {
  const response = await request(getMiddleware(serverSettings))
    .get('/health/check')
    .expect(200);

  const { mountPath, ...buildSettings } = serverSettings;

  expect(response.body).toEqual(buildSettings);
});

test('should mount GraphQL endpoint to configured path', async () => {
  const mountPath = '/my-custom-path/anywhere';

  const server = getMiddleware({
    ...serverSettings,
    mountPath
  });

  server.get(mountPath, (req, res) => {
    res.status(200).send({ message: 'Custom endpoint' });
  });

  const response = await request(server).get(mountPath).expect(200);

  expect(response.body).toEqual({ message: 'Custom endpoint' });
});
