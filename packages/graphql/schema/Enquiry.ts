import { gql } from 'apollo-server';

export default gql`
  type Enquiry {
    id: Int!
    type: Int
    location: String
    status: Int
    date: Int # added_at
    hold_period_hours: Int
    total_value: Float # value
    pax: Int
    status_label: String
    completed: Boolean # 🚧
    per_person_currency: String # currency
    tour: Tour #
    traveller_name: String # 🚧 traveller_id
    last_commented_at: Int # 🚧
    has_payment_token: Boolean # 🚧
    book_now_url: String # conversation.url
    code: String # user.code 🚧 book_now_url|invoice_download_url
    has_operator_replied: Boolean # 🚧
  }
`;
