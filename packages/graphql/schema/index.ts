import { gql } from 'apollo-server';

import Auth from './Auth';
import Enquiry from './Enquiry';
import Tour from './Tour';
import Video from './Video';

const typeDefs = gql`
  ${Auth}
  ${Video}
  ${Enquiry}
  ${Tour}

  type Query {
    info: String!
    videos(params: VideoQuery): [Video!]!
    video(id: ID!): Video
    enquiries: [Enquiry!]!
  }

  type Mutation {
    login(auth: LoginPayload!): Session!
    createVideo(video: VideoInput!): Video
    updateVideo(id: ID!, video: VideoInput!): Video
    deleteVideo(id: ID!): Boolean!
  }
`;

export default typeDefs;
