import winston from 'winston';

const { combine, timestamp, simple, splat } = winston.format;

interface LogLevelObject {
  name: string;
  level: number;
}

interface WinstonLevels {
  error: LogLevelObject;
  warn: LogLevelObject;
  info: LogLevelObject;
  verbose: LogLevelObject;
  debug: LogLevelObject;
  silly: LogLevelObject;
}

export type LoggingLevel = keyof WinstonLevels;

export const LOG_MESSAGES = {
  apiError: 'External API error',
  unauth: 'Unauthenticated',
  appLogin: 'App login error',
  invalidInput: 'Invalid user input'
};

export const createLogEntry = (
  level: LoggingLevel,
  message: string,
  log?: any
) => {
  const logger = winston.loggers.get('main');
  logger[level](message, log);
};

const createLogger = () => {
  const transports = [new winston.transports.Console()];
  const format = combine(splat(), timestamp(), simple());

  winston.loggers.add('main', {
    format,
    transports
  });
};

export default createLogger;
