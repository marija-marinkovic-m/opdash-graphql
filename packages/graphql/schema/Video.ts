import { gql } from 'apollo-server';

export default gql`
  input VideoQuery {
    limit: Int
    offset: Int
    with: String
  }
  type VideoTourCity {
    name: String
  }
  type VideoTour {
    id: Int!
    name: String
    length: Int
    endCity: VideoTourCity
    startCity: VideoTourCity
  }
  type Video {
    id: ID!
    operatorId: Int
    headline: String
    message: String
    active: Boolean
    added: Int
    tours: [VideoTour!]!
  }
  input VideoTourInput {
    id: Int
  }
  input VideoInput {
    active: Boolean!
    message: String!
    headline: String!
    operatorId: Int
    tours: [VideoTourInput!]
  }
`;
