import { merge } from 'lodash';

import AuthAPI, { BasicLoginInput } from './auth';

const mockSession = {
  user: {
    id: 250,
    username: 'myUserName',
    email: null
  },
  token:
    'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1OTA2NjQ5MzEsImV4cCI6MTU5MDcwMDkzMSwianRpIjoiVnBLN29BTUpZc2J4UEwzbCIsInN1YiI6MjUwLCJpc0FkbWluIjp0cnVlLCJpc1RlYW1NZW1iZXIiOnRydWUsInVzZXJuYW1lIjoiY2lwcmlhbiIsInRlYW1NZW1iZXJJZCI6MjUwLCJyb2xlIjpudWxsLCJpc01haW5PcGVyYXRvclVzZXIiOmZhbHNlfQ.7CrO7FOntmTiMCgHlhmVLJ1sJ_jc8YG8cRwAnthDsxE',
  isAdmin: true,
  isTeamMember: true
};

interface TestAuthAPI extends Omit<AuthAPI, 'post'> {
  post: jest.Mock;
}

const mocks = {
  post: jest.fn()
};

const authApi = new AuthAPI();
const ds: TestAuthAPI = merge(authApi, mocks);

describe('[AuthAPI.login]', () => {
  it('should request post and return session on success', async () => {
    ds.post.mockReturnValueOnce(mockSession);
    const params = {
      username: 'myUserName',
      password: '123'
    };
    const res = await ds.login(params);

    expect(res).toEqual(mockSession);
    expect(mocks.post).toBeCalledWith('auth/login', params);
  });
});
