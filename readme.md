# OpDash GraphQL project

This repository consists of multiple workspaces. In the context of the workspace yarn feature, a workspace is specific package stored within the project worktrees.

## Worktree structure

Worktrees declared within the *OpDash GraphQL* project are structured as follows:

```
./
└── packages/
    ├── client/
    │   └── .next/
    ├── graphql/
    │   └── dist/
    └── server/
        └── index.ts
```

### Client App

The client app build is aimed to be statically generated and server-rendered consumer of data provided by the adjacent GraphQL API. Some of the features:

* customizable server, universal rendering, filesystem based routing,... powered by [Next.js](https://nextjs.org/)
* secure authorization using `httpOnly` cookie for managing the JWT token (as oposed to the practice storing sensitive session information in local storage)
* `@tourradar/tr-design-tokens` applied to UI (exploring [_scout-design-system_](https://bitbucket.org/tourradar/scout-design-system/src/master/))

### GraphQL API

The graphql package is constructed to produce self-documenting GraphQL API using data from multiple datasources: internal [restapi](https://bitbucket.org/tourradar/restapi/src/master/), [enquiries-api](https://bitbucket.org/tourradar/enquiries-api/src/master/),... and even the [web](https://bitbucket.org/tourradar/web/src/master/). Some of the features:

* interactive, in-browser GraphQL IDE to explore the schema
* CLI tool for generating TypeScript typings out of a GraphQL schema - these typings can then be cross-referenced between workspaces (see client pages for example)
* custom logging (on top of [winston](https://www.npmjs.com/package/winston))

### Server

The server workspace is used to stitch everything together. It uses `ts-node` to spin both GraphQL API and client app on production, it will compile it and cache the build.

## Requirements

1. node^12
2. yarn^1.2
2. typescript^3.9
3. eslint^6.8

## Installation on local enviroment

```bash
# setup the env vars using the example
$ cp .env.example .env

# install packages
# make sure you have .npmrc configured (cp .npmrc_config .npmrc)
# by replacing NPM_TOKEN with TR private npm token - from 1Password
$ yarn

# run the build
$ yarn build && yarn start

# develop
$ yarn dev
```