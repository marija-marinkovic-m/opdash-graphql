export enum Environment {
  staging = 'staging',
  production = 'production',
  development = 'development'
}

export default {
  graphqlPath: `/${process.env.GRAPHQL_PATH || 'graphql'}`,
  environment: (process.env.NODE_ENV as Environment) || '',
  appName: 'opdash-grapql',
  appVersion: process.env.APP_VERSION || '',
  appPort: process.env.PORT || process.env.APP_PORT || 4000,
  stAuth: process.env.XSTAUTH,
  stAuthHeader: 'X-ST-AUTH',
  internalApiKey: process.env.INTERNAL_API_KEY
};
