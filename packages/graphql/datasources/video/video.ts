import { get, omit, pick } from 'lodash';

import InternalRestAPI from '../rest';
interface VideoModel {
  id: number;
  active: boolean;
  operator_id: number;
  headline: string;
  added?: number;
  message?: string;
  tours?: TourModel[];
}

interface TourCity {
  name: string;
}
interface TourModel {
  id: number;
  name: string;
  length: number;
  end_city: TourCity;
  start_city: TourCity;
}
export interface VideoInput {
  active: boolean;
  message: string;
  headline: string;
  operatorId?: number;
  tours?: { id: number }[] | 'all';
}
export default class VideoAPI extends InternalRestAPI {
  async getVideos(params?: { [key: string]: string | number }) {
    const response = await this.get<VideoModel[]>('videos', params);
    return response && Array.isArray(response)
      ? response.map(video => this.videoReducer(video))
      : [];
  }

  async getVideo({ videoId }) {
    const response = await this.get<VideoModel>(`videos/${videoId}`);
    return response && this.videoReducer(response);
  }

  async createVideo(body: VideoInput) {
    const response = await this.post<VideoModel>('videos', body);
    return response && this.videoReducer(response);
  }

  async updateVideo(id: number, body: VideoInput) {
    const response = await this.put<VideoModel>(`videos/${id}`, body);
    return response && this.videoReducer(response);
  }

  async deleteVideo(id: number) {
    const response = await this.delete(`videos/${id}`);
    return response && response.deleted;
  }

  videoReducer(video: VideoModel) {
    return {
      operatorId: get(video, 'operator_id'),
      tours: this.videoToursReducer(get(video, 'tours', [])),
      ...omit(video, ['operator_id', 'tours'])
    };
  }

  videoToursReducer(tours: TourModel[]) {
    return (
      tours &&
      tours.map(tour => ({
        id: tour.id,
        name: tour.name,
        length: tour.length,
        startCity: tour.start_city && pick(tour.start_city, ['id', 'name']),
        endCity: tour.end_city && pick(tour.end_city, ['id', 'name'])
      }))
    );
  }
}
