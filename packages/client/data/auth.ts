import gql from 'graphql-tag';

export const AUTH_LOGIN = gql`
  mutation LogIn($username: String!, $password: String!) {
    login(auth: { username: $username, password: $password }) {
      user {
        id
        email
        username
      }
      token
      isAdmin
      isTeamMember
    }
  }
`;
