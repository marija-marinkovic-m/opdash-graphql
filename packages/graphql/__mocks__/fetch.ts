jest.mock('node-fetch', () => {
  const fetchMock = require('jest-fetch-mock');
  const { Headers, Request, Response } = require('jest-fetch-mock');

  return { default: fetchMock, Headers, Request, Response };
});
