import { RequestOptions } from 'apollo-datasource-rest';

import config from '../config';

import RestAPI from './base';

export default class InternalRestAPI extends RestAPI {
  baseURL = `${process.env.DATASOURCE_REST_API}/v2/`;

  willSendRequest(request: RequestOptions) {
    if (config.stAuth) {
      request.headers.set(config.stAuthHeader, config.stAuth);
    }
    request.headers.set('Authorization', `Bearer ${this.context.authToken}`);
    super.willSendRequest(request);
  }
}
