require('dotenv').config();

module.exports = {
  env: {
    APP_PORT: process.env.PORT || process.env.APP_PORT || 4000,
    GRAPHQL_URI: process.env.GRAPHQL_URI
  }
};
