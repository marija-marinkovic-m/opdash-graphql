import { ApolloError } from 'apollo-server';

import { LOG_MESSAGES } from '../logger';

export const throwApiError = (error: ApolloError) => {
  error.message = LOG_MESSAGES.apiError;
  throw error;
};
