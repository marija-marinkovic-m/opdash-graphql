import { Config } from 'apollo-server';
import { createTestClient } from 'apollo-server-testing';

import { createServer } from '../server';

const createMockServer = (config: Config) => {
  const server: any = createServer(config);
  return createTestClient(server);
};

export default createMockServer;
