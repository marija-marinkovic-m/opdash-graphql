import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import fetch from 'isomorphic-unfetch';

const createApolloClient = (initialState, ctx) => {
  return new ApolloClient({
    ssrMode: Boolean(ctx),
    link: createHttpLink({
      uri: process.env.GRAPHQL_URI,
      credentials: 'same-origin',
      fetch
    }),
    cache: new InMemoryCache().restore(initialState)
  });
};

export default createApolloClient;
