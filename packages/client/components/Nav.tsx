import React, { FunctionComponent } from 'react';

import cn from 'classnames';
import unfetch from 'isomorphic-unfetch';
import Link from 'next/link';
import { useRouter } from 'next/router';

import styles from './Nav.module.scss';

interface Props {
  loggedIn: boolean;
}

const PREFIX = 'scout-nav';
const labelStyle = styles['scout-label'];

const Nav: FunctionComponent<Props> = ({ loggedIn }) => {
  const router = useRouter();
  return (
    <ul className={styles[PREFIX]}>
      <li>
        <Link href="/">
          <a
            className={cn(labelStyle, {
              [styles['scout-label__active']]: router.pathname === '/'
            })}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              height="24"
              viewBox="0 0 24 24"
              width="24"
              className={styles[`${PREFIX}__action-icon`]}
            >
              <path d="M0 0h24v24H0V0z" fill="none" />
              <path d="M12 5.69l5 4.5V18h-2v-6H9v6H7v-7.81l5-4.5M12 3L2 12h3v8h6v-6h2v6h6v-8h3L12 3z" />
            </svg>
            Home
          </a>
        </Link>
      </li>
      <li>
        <Link href="/nossr">
          <a
            className={cn(labelStyle, {
              [styles['scout-label__active']]: router.pathname === '/nossr'
            })}
            title="Protected"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              height="24"
              viewBox="0 0 24 24"
              width="24"
              className={styles[`${PREFIX}__action-icon`]}
            >
              <g fill="none">
                <path d="M0 0h24v24H0V0z" />
                <path d="M0 0h24v24H0V0z" opacity=".87" />
              </g>
              <path d="M18 8h-1V6c0-2.76-2.24-5-5-5S7 3.24 7 6v2H6c-1.1 0-2 .9-2 2v10c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V10c0-1.1-.9-2-2-2zM9 6c0-1.66 1.34-3 3-3s3 1.34 3 3v2H9V6zm9 14H6V10h12v10zm-6-3c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2z" />
            </svg>
            No SSR
          </a>
        </Link>
      </li>
      <li>
        <Link href="/ssr">
          <a
            className={cn(labelStyle, {
              [styles['scout-label__active']]: router.pathname === '/ssr'
            })}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              height="24"
              viewBox="0 0 24 24"
              width="24"
              className={styles[`${PREFIX}__action-icon`]}
              xlinkTitle="Protected"
            >
              <g fill="none">
                <path d="M0 0h24v24H0V0z" />
                <path d="M0 0h24v24H0V0z" opacity=".87" />
              </g>
              <path d="M18 8h-1V6c0-2.76-2.24-5-5-5S7 3.24 7 6v2H6c-1.1 0-2 .9-2 2v10c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V10c0-1.1-.9-2-2-2zM9 6c0-1.66 1.34-3 3-3s3 1.34 3 3v2H9V6zm9 14H6V10h12v10zm-6-3c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2z" />
            </svg>
            SSR
          </a>
        </Link>
      </li>
      <li>
        {loggedIn ? (
          <a
            className={labelStyle}
            onClick={e => {
              e.preventDefault();
              unfetch('/api/token', {
                method: 'DELETE'
              })
                .then(r => r.json())
                .then(res => alert(JSON.stringify(res, null, 2)))
                .finally(() => router.push('/'));
            }}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              height="24"
              viewBox="0 0 24 24"
              width="24"
              className={styles[`${PREFIX}__action-icon`]}
            >
              <path d="M0 0h24v24H0V0z" fill="none" />
              <path d="M22 19h-6v-4h-2.68c-1.14 2.42-3.6 4-6.32 4-3.86 0-7-3.14-7-7s3.14-7 7-7c2.72 0 5.17 1.58 6.32 4H24v6h-2v4zm-4-2h2v-4h2v-2H11.94l-.23-.67C11.01 8.34 9.11 7 7 7c-2.76 0-5 2.24-5 5s2.24 5 5 5c2.11 0 4.01-1.34 4.71-3.33l.23-.67H18v4zM7 15c-1.65 0-3-1.35-3-3s1.35-3 3-3 3 1.35 3 3-1.35 3-3 3zm0-4c-.55 0-1 .45-1 1s.45 1 1 1 1-.45 1-1-.45-1-1-1z" />
            </svg>
            Log out
          </a>
        ) : (
          <Link href="/login">
            <a
              className={cn(labelStyle, {
                [styles['scout-label__active']]: router.pathname === '/login'
              })}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height="24"
                viewBox="0 0 24 24"
                width="24"
                className={styles[`${PREFIX}__action-icon`]}
              >
                <path d="M0 0h24v24H0V0z" fill="none" />
                <path d="M22 19h-6v-4h-2.68c-1.14 2.42-3.6 4-6.32 4-3.86 0-7-3.14-7-7s3.14-7 7-7c2.72 0 5.17 1.58 6.32 4H24v6h-2v4zm-4-2h2v-4h2v-2H11.94l-.23-.67C11.01 8.34 9.11 7 7 7c-2.76 0-5 2.24-5 5s2.24 5 5 5c2.11 0 4.01-1.34 4.71-3.33l.23-.67H18v4zM7 15c-1.65 0-3-1.35-3-3s1.35-3 3-3 3 1.35 3 3-1.35 3-3 3zm0-4c-.55 0-1 .45-1 1s.45 1 1 1 1-.45 1-1-.45-1-1-1z" />
              </svg>
              Login
            </a>
          </Link>
        )}
      </li>
    </ul>
  );
};

export default Nav;
