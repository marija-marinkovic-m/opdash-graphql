import { gql } from 'apollo-server';

export default gql`
  type SessionUser {
    id: Int!
    username: String!
    email: String
  }

  type Session {
    user: SessionUser!
    token: String!
    isAdmin: Boolean!
    isTeamMember: Boolean!
  }

  input LoginPayload {
    username: String!
    password: String!
  }
`;
