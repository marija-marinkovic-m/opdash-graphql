import { NextApiRequest, NextApiResponse, NextApiHandler } from 'next';
import { parseCookies, setCookie, destroyCookie } from 'nookies';

import AuthToken, { AuthData, TOKEN_PATH } from '../../services/auth-token';

const Token: NextApiHandler = (
  req: NextApiRequest,
  res: NextApiResponse<AuthData>
) => {
  switch (req.method) {
    case 'POST': {
      try {
        const body = JSON.parse(req.body);
        setCookie({ res }, TOKEN_PATH, body.authToken, {
          maxAge: 30 * 24 * 60 * 60,
          path: '/',
          httpOnly: true
        });
        res.status(200).json({ loggedIn: true });
      } catch (e) {
        res
          .status(422)
          .json({ loggedIn: false, message: 'Could not find the token' });
      }
      break;
    }
    case 'GET': {
      const auth = new AuthToken(parseCookies({ req }));

      res.status(200).json({
        loggedIn: auth.isValid
      });
      break;
    }
    case 'DELETE': {
      destroyCookie({ res }, TOKEN_PATH, {
        path: '/'
      });
      res.status(200).json({
        loggedIn: false,
        message: 'Successfully logged out'
      });
      break;
    }
    default: {
      res.status(422).json({ loggedIn: false, message: 'Bad Method' });
    }
  }
};

export default Token;
