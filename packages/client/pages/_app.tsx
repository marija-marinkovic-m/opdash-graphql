import '../styles/index.scss';

import React from 'react';

import App, { AppProps, AppContext } from 'next/app';
import Router from 'next/router';
import NProgress from 'nprogress';

import Layout from '../components/Layout';
import { AuthData, checkUserAuth } from '../services/auth-token';

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

const TheApp = ({
  Component,
  pageProps,
  auth
}: AppProps & { auth: AuthData }) => {
  return (
    <Layout auth={auth}>
      <Component {...pageProps} auth={auth} />
    </Layout>
  );
};

TheApp.getInitialProps = async (appContext: AppContext) => {
  const auth = await checkUserAuth(appContext.ctx);
  const appProps = await App.getInitialProps(appContext);
  return { auth, ...appProps };
};

export default TheApp;
