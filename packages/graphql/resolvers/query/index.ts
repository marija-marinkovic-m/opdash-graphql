import { ResolverFn } from 'apollo-server';

const Query: { [key: string]: ResolverFn } = {
  videos: (_, request, { dataSources: { videoAPI } }) => {
    return videoAPI.getVideos(request.params);
  },
  video: (_, { id }, { dataSources: { videoAPI } }) => {
    return videoAPI.getVideo({ videoId: id });
  },
  enquiries: (_, __, { dataSources: { enquiryAPI } }) => {
    return enquiryAPI.getEnquiries();
  }
};

export default Query;
