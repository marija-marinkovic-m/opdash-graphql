import React from 'react';

import { useQuery } from '@apollo/react-hooks';
import { Video, VideoQuery } from '@opdash-graphql/graphql/__generated__/types';
import { compose } from 'recompose';

import Button from '../components/Button';
import { GET_VIDEOS_QUERY } from '../data/video';
import { withApollo } from '../lib/apollo';
import withPrivateRoute from '../lib/private-route';

const NOSSR = () => {
  const { data, loading, error, refetch } = useQuery<
    { videos: Video },
    VideoQuery
  >(GET_VIDEOS_QUERY, {
    variables: {
      limit: 2,
      offset: 0,
      with: 'tours,tours.startCity,tours.endCity'
    }
  });

  if (loading) {
    return <p>Loading...</p>;
  }
  if (error) {
    return <p>Error: {error.message}</p>;
  }

  return (
    <>
      <h1>This is rendered client side</h1>
      <p>(Private)</p>
      <pre>Data: {JSON.stringify(data.videos, null, 2)}</pre>
      <Button onClick={() => refetch()}>Refetch</Button>
    </>
  );
};

export default compose(withApollo({ ssr: false }), withPrivateRoute)(NOSSR);
