export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
};

export type SessionUser = {
  __typename?: 'SessionUser';
  id: Scalars['Int'];
  username: Scalars['String'];
  email?: Maybe<Scalars['String']>;
};

export type Session = {
  __typename?: 'Session';
  user: SessionUser;
  token: Scalars['String'];
  isAdmin: Scalars['Boolean'];
  isTeamMember: Scalars['Boolean'];
};

export type LoginPayload = {
  username: Scalars['String'];
  password: Scalars['String'];
};

export type VideoQuery = {
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  with?: Maybe<Scalars['String']>;
};

export type VideoTourCity = {
  __typename?: 'VideoTourCity';
  name?: Maybe<Scalars['String']>;
};

export type VideoTour = {
  __typename?: 'VideoTour';
  id: Scalars['Int'];
  name?: Maybe<Scalars['String']>;
  length?: Maybe<Scalars['Int']>;
  endCity?: Maybe<VideoTourCity>;
  startCity?: Maybe<VideoTourCity>;
};

export type Video = {
  __typename?: 'Video';
  id: Scalars['ID'];
  operatorId?: Maybe<Scalars['Int']>;
  headline?: Maybe<Scalars['String']>;
  message?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  added?: Maybe<Scalars['Int']>;
  tours: Array<VideoTour>;
};

export type VideoTourInput = {
  id?: Maybe<Scalars['Int']>;
};

export type VideoInput = {
  active: Scalars['Boolean'];
  message: Scalars['String'];
  headline: Scalars['String'];
  operatorId?: Maybe<Scalars['Int']>;
  tours?: Maybe<Array<VideoTourInput>>;
};

export type Enquiry = {
  __typename?: 'Enquiry';
  id: Scalars['Int'];
  type?: Maybe<Scalars['Int']>;
  location?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['Int']>;
  date?: Maybe<Scalars['Int']>;
  hold_period_hours?: Maybe<Scalars['Int']>;
  total_value?: Maybe<Scalars['Float']>;
  pax?: Maybe<Scalars['Int']>;
  status_label?: Maybe<Scalars['String']>;
  completed?: Maybe<Scalars['Boolean']>;
  per_person_currency?: Maybe<Scalars['String']>;
  tour?: Maybe<Tour>;
  traveller_name?: Maybe<Scalars['String']>;
  last_commented_at?: Maybe<Scalars['Int']>;
  has_payment_token?: Maybe<Scalars['Boolean']>;
  book_now_url?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  has_operator_replied?: Maybe<Scalars['Boolean']>;
};

export type Tour = {
  __typename?: 'Tour';
  name?: Maybe<Scalars['String']>;
};

export type Query = {
  __typename?: 'Query';
  info: Scalars['String'];
  videos: Array<Video>;
  video?: Maybe<Video>;
  enquiries: Array<Enquiry>;
};


export type QueryVideosArgs = {
  params?: Maybe<VideoQuery>;
};


export type QueryVideoArgs = {
  id: Scalars['ID'];
};

export type Mutation = {
  __typename?: 'Mutation';
  login: Session;
  createVideo?: Maybe<Video>;
  updateVideo?: Maybe<Video>;
  deleteVideo: Scalars['Boolean'];
};


export type MutationLoginArgs = {
  auth: LoginPayload;
};


export type MutationCreateVideoArgs = {
  video: VideoInput;
};


export type MutationUpdateVideoArgs = {
  id: Scalars['ID'];
  video: VideoInput;
};


export type MutationDeleteVideoArgs = {
  id: Scalars['ID'];
};

export enum CacheControlScope {
  Public = 'PUBLIC',
  Private = 'PRIVATE'
}

