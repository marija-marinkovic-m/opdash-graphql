import config from './config';
import createLogger, { createLogEntry } from './logger';
import { createServer } from './server';
import createMiddleware from './server/middleware';

const main = () => {
  createLogger();
  const server = createServer();
  const app = createMiddleware({
    mountPath: config.graphqlPath,
    version: config.appVersion,
    environment: config.environment,
    build_number: process.env.PIPELINE_BUILD_NUMBER || ''
  });

  server.applyMiddleware({ app, path: server.graphqlPath });

  app.listen(
    {
      port: config.appPort
    },
    () => {
      createLogEntry(
        'info',
        `🚀 Server ready at http://localhost:${config.appPort}${server.graphqlPath}`
      );
    }
  );

  return {
    app
  };
};

export default main;
