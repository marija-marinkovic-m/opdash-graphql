import gql from 'graphql-tag';

export const GET_VIDEOS_QUERY = gql`
  query GetVideos($limit: Int, $offset: Int, $with: String) {
    videos(params: { limit: $limit, offset: $offset, with: $with }) {
      id
      headline
      operatorId
      active
      tours {
        id
        startCity {
          name
        }
        endCity {
          name
        }
      }
    }
  }
`;
