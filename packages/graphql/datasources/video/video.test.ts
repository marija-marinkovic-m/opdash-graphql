/* eslint-disable @typescript-eslint/no-use-before-define */
import { merge } from 'lodash';

import VideoAPI, { VideoInput } from './video';

interface TestVideoAPI
  extends Omit<VideoAPI, 'get' | 'post' | 'put' | 'delete'> {
  get: jest.Mock;
  post: jest.Mock;
  put: jest.Mock;
  delete: jest.Mock;
}
const mocks = {
  get: jest.fn(),
  post: jest.fn(),
  put: jest.fn(),
  delete: jest.fn()
};

const videoApi = new VideoAPI();
const ds: TestVideoAPI = merge(videoApi, mocks);

describe('[VideoAPI.videoReducer]', () => {
  it('should properly transform video', () => {
    expect(ds.videoReducer(mockVideoResponse)).toEqual(mockVideo);
  });
});

describe('[VideoAPI.getVideos]', () => {
  it('should look up videos from api', async () => {
    ds.get.mockReturnValueOnce([mockVideoResponse]);
    const res = await ds.getVideos();
    expect(res).toEqual([mockVideo]);
    expect(mocks.get).toBeCalledWith('videos', undefined);
  });

  it('should accept parameters', async () => {
    const params = {
      with: 'tours,tours.startCity,tours.endCity',
      limit: 1,
      offset: 2
    };
    ds.get.mockReturnValueOnce([mockVideoResponse]);
    const res = await ds.getVideos(params);
    expect(res).toEqual([mockVideo]);
    expect(mocks.get).toBeCalledWith('videos', params);
  });
});

describe('[VideoAPI.getVideo]', () => {
  it('should look up single video from api', async () => {
    const videoId = 1;
    mocks.get.mockReturnValueOnce(mockVideoResponse);
    const res = await ds.getVideo({ videoId });

    expect(res).toEqual(mockVideo);
    expect(mocks.get).toBeCalledWith(`videos/${videoId}`);
  });
});

describe('[VideoAPI.createVideo]', () => {
  it('should request post with provided input as body', async () => {
    const input: VideoInput = {
      headline: 'Headline Test',
      message: '<embed></embed>',
      active: false,
      tours: [{ id: 1 }]
    };
    const rawResponse = {
      ...input,
      id: 123,
      operator_id: 166
    };
    const response = {
      ...input,
      id: 123,
      operatorId: 166
    };
    mocks.post.mockReturnValueOnce(rawResponse);
    const res = await ds.createVideo(input);

    expect(res).toEqual(response);
    expect(mocks.post).toBeCalledWith('videos', input);
  });
});

describe('[VideoAPI.updateVideo]', () => {
  it('should request put with provided data', async () => {
    mocks.put.mockReturnValueOnce(mockVideoResponse);
    const res = await ds.updateVideo(mockVideo.id, mockVideo);

    expect(res).toEqual(mockVideo);
    expect(mocks.put).toBeCalledWith(`videos/${mockVideo.id}`, mockVideo);
  });
});

describe('[VideoAPI.deleteVideo]', () => {
  it('should request delete with correct arguments', async () => {
    const videoId = 123;
    mocks.delete.mockResolvedValueOnce({ deleted: true });
    const res = await ds.deleteVideo(videoId);

    expect(res).toEqual(true);
    expect(mocks.delete).toBeCalledWith(`videos/${videoId}`);
  });
});

const mockVideo = {
  id: 5422,
  headline: 'Ecuador & Galapagos - Topdeck Travel',
  message: '<iframe />',
  operatorId: 203,
  active: true,
  added: 1575080927,
  tours: [
    {
      id: 166371,
      name: 'Grampians Wilderness Escape - 1 Day',
      length: 1,
      startCity: {
        id: 4995,
        name: 'Melbourne'
      },
      endCity: {
        id: 4995,
        name: 'Melbourne'
      }
    }
  ]
};
const mockVideoResponse = {
  id: 5422,
  headline: 'Ecuador & Galapagos - Topdeck Travel',
  message: '<iframe />',
  operator_id: 203,
  active: true,
  added: 1575080927,
  tours: [
    {
      id: 166371,
      name: 'Grampians Wilderness Escape - 1 Day',
      name_de: '',
      operator_id: 166,
      is_custom: false,
      itinerary_type: 'f',
      code: 'AT-4',
      special_event: 0,
      description:
        '<p>Journey with us to the Grampians or Gariwerd, one of Australia’s ancient and spectacular mountain ranges. Enjoy the breathtaking views and amazing landscapes. Learn and appreciate the rich Aboriginal heritage. Encounter native wildlife and fascinatingly lush vegetation of the Grampians National Park. In this Grampians Tour, witness the spectacles of sandstone ridges, rugged mountain ranges, panoramic views, picturesque waterfalls, and the bustling plant and animal life. </p>',
      description_de: '',
      description_short: null,
      itinerary:
        '<h4>1 Day Grampians National Park & Wilderness Escape</h4><p><strong>Operates </strong>*Mon, Tues, *Wed, Fri, Sun   (*seasonal departures)</p>',
      keywords: '',
      supplement: false,
      suspended: false,
      friendly: true,
      prize_pool: false,
      length_type: 'd',
      length: 1,
      max_group: 24,
      age_group: 0,
      min_age: 8,
      max_age: 70,
      website:
        'https://autopiatours.com.au/eco-tours-australia/melbourne/grampians-tour/',
      pdf: '',
      start_city_id: 4995,
      end_city_id: 4995,
      time: 1560611540,
      updated: 1586510413,
      dropped: false,
      listing_status: 'l',
      content_status: '1',
      reviews_count: 0,
      reviews_rating: 0,
      messages_count: 0,
      physical_demand: 0,
      databug_valid_time: null,
      auto_confirm: true,
      highlights:
        'Enjoy the breathtaking views and amazing landscapes\nLearn and appreciate the rich Aboriginal heritage\nEncounter native wildlife of the Grampians National Park\nWitness the spectacles of ridges and mountain ranges\nEmbark on bushland hiking trails with the best views ',
      map_id: 1159144,
      main_accommodation_type: null,
      main_transport_type: null,
      main_pace: 'Standard',
      small_group: false,
      all_inclusive: true,
      budget: 'Budget',
      is_free_quote: 0,
      start_time: '07:15',
      end_time: '20:15 - 20:30',
      group_id: null,
      is_clean: 0,
      start_point_id: 19154,
      end_point_id: 19141,
      target_min_age: null,
      target_max_age: null,
      comfort_level: null,
      min_group: 0,
      type_id: 237,
      url: 'https://staging.tourradar.com/t/166371',
      active: true,
      start_city: {
        id: 4995,
        name: 'Melbourne',
        state_id: 1597,
        lng: 144.965099,
        lat: -37.815603
      },
      end_city: {
        id: 4995,
        name: 'Melbourne',
        state_id: 1597,
        lng: 144.965099,
        lat: -37.815603
      }
    }
  ]
};
