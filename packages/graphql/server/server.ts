import { ApolloServer, Config } from 'apollo-server-express';

// import config, { Environment } from '../config';
import { VideoAPI, EnquiryAPI, AuthAPI } from '../datasources';
import resolvers from '../resolvers';
import typeDefs from '../schema';

// const isDev =
//   [Environment.development, Environment.staging].indexOf(config.environment) >
//   -1;

const createServer = (serverConfig: Config = {}) =>
  new ApolloServer({
    ...serverConfig,
    typeDefs,
    resolvers,
    introspection: true,
    playground: true,
    context: ({ req }) => ({
      authToken: req.headers['x-auth-token'],
      appName: req.headers['x-app-name'],
      appVersion: req.headers['x-app-version'],
      ipAddress: req.headers['x-forwarded-for']
    }),
    dataSources: () => ({
      authAPI: new AuthAPI(),
      videoAPI: new VideoAPI(),
      enquiryAPI: new EnquiryAPI()
    })
  });

export default createServer;
