import cases from 'jest-in-case';
import { loggers } from 'winston';

import { createLogEntry, LoggingLevel } from './logger';

describe('[logger]', () => {
  cases(
    'should trigger for provided level with proper params',
    (opts: { params: [LoggingLevel, string, any] }) => {
      const [level, message, log] = opts.params;
      createLogEntry(level, message, log);

      expect(loggers.get('')[level]).toBeCalledWith(message, log);
    },
    {
      'info without message': { params: ['info'] },
      'info without log param': { params: ['info', 'Message Test'] },
      'info printed': { params: ['info', 'Message Test', { source: 'test' }] },
      'error level': { params: ['error', 'Invalid'] },
      'warn level': { params: ['warn', 'Attention'] },
      debug: { params: ['debug'] }
    }
  );
});
