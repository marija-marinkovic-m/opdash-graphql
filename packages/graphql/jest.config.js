module.exports = {
  modulePathIgnorePatterns: ['<rootDir>/dist/'],
  preset: 'ts-jest',
  setupFiles: ['./jest/setupTests.ts'],
  testEnvironment: 'node',
  testMatch: ['**/*.test.ts?(x)'],
  watchPlugins: [
    'jest-watch-typeahead/filename',
    'jest-watch-typeahead/testname'
  ]
};
