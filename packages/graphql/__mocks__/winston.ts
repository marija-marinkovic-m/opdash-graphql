jest.mock('winston', () => {
  const format = {
    combine: jest.fn(),
    timestamp: jest.fn(),
    simple: jest.fn(),
    splat: jest.fn()
  };

  const transports = {
    Console: jest.fn()
  };

  const logger = {
    info: jest.fn(),
    error: jest.fn(),
    warn: jest.fn(),
    verbose: jest.fn(),
    debug: jest.fn(),
    silly: jest.fn(),
    log: jest.fn()
  };

  const loggers = {
    add: jest.fn(() => logger),
    get: jest.fn(() => logger)
  };

  return {
    format,
    transports,
    loggers
  };
});
