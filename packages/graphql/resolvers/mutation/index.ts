import { ResolverFn } from 'apollo-server';

const Mutation: { [key: string]: ResolverFn } = {
  login: (
    _,
    { auth: { username, password } },
    { dataSources: { authAPI } }
  ) => {
    return authAPI.login({ username, password });
  },
  createVideo: (_, { video }, { dataSources: { videoAPI } }) => {
    return videoAPI.createVideo({ ...video }); // https://github.com/apollographql/apollo-server/issues/1539
  },
  updateVideo: (_, { id, video }, { dataSources: { videoAPI } }) => {
    return videoAPI.updateVideo(id, { ...video });
  },
  deleteVideo: (_, { id }, { dataSources: { videoAPI } }) => {
    return videoAPI.deleteVideo(id);
  }
};

export default Mutation;
